#!/bin/sh

# Over 1 year, keep only 1 backup per 6 months
find -ctime +365 | grep -Ev -- '-0[16]-15\.' | xargs rm -fv

# Over 6 months, keep only 1 backup per month 
find -ctime +183 | grep -v -- '-15.' | xargs rm -fv

# Over 1 month, keep only 2 backups per month
find -ctime +31 | grep -Ev -- '-(1|15)\.' | xargs rm -fv

# Over 1 week, keep only 1 backup per week
find -ctime +8 | grep -Ev -- '-(1|8|15|22|29)\.' | xargs rm -fv
